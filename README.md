# ES6 Helpers

## Intro

Just a basic bunch of ES6 helpers & utilities with a variety of uses.

## Transpilation

The source code is in `/src`, however, the transpiled code is in the `/` (root) dir.

### Development

In order to do dev without a hassle, try this:

1. Say you're working on a feature 'cool', branch out as you would eg. `feature/cool`
2. Set this repo up for linking: `yarn link`
3. Kick off a transpile watch: `yarn run watch`. This will transpile for you everytime you make changes in `/src`
4. Go to the repo you want to test the current local version of your helpers on and: `yarn link es6-helpers`
5. When you're done: `yarn unlink es6-helpers` in the other repo & then `yarn` when ready to get the latest remote version of the helpers back.

### Making Final Changes

Every time you make a change and want to publish this, please make sure you run `yarn build` after to kick off a babel transpilation from `/src`.

## Using This Repo

This repo is not a published NPM module, however, you can easily do a `yarn add https://bitbucket.org/digitaldialogue/es6-helpers.git#{COMMIT_HASH}`
Making sure to replace `{COMMIT_HASH}` with an actual commit hash you'd like to peg to.
When you'd like to update, you have to update this commit hash in your `package.json` then do a `yarn`.
