const subYears = require('date-fns/sub_years');
const startOfISOWeek = require('date-fns/start_of_iso_week');
const endOfISOWeek = require('date-fns/end_of_iso_week');
const setISOWeek = require('date-fns/set_iso_week');
const getISOWeek = require('date-fns/get_iso_week');

const pipe = require('../../functional/pipe');
const curryRight = require('../../functional/curryRight');


const sameWeeksPreviousYear = (range, yearsBack = 1) => ({
  StartTime: pipe(
    curryRight(subYears, yearsBack),
    curryRight(setISOWeek, getISOWeek(range.StartTime)),
    startOfISOWeek,
  )(range.StartTime),
  EndTime: pipe(
    curryRight(subYears, yearsBack),
    curryRight(setISOWeek, getISOWeek(range.EndTime)),
    endOfISOWeek,
  )(range.EndTime),
});


module.exports = sameWeeksPreviousYear;
