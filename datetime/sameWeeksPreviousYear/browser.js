"use strict";

var subYears = require('date-fns/sub_years');

var startOfISOWeek = require('date-fns/start_of_iso_week');

var endOfISOWeek = require('date-fns/end_of_iso_week');

var setISOWeek = require('date-fns/set_iso_week');

var getISOWeek = require('date-fns/get_iso_week');

var pipe = require('../../functional/pipe');

var curryRight = require('../../functional/curryRight');

var sameWeeksPreviousYear = function sameWeeksPreviousYear(range) {
  var yearsBack = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  return {
    StartTime: pipe(curryRight(subYears, yearsBack), curryRight(setISOWeek, getISOWeek(range.StartTime)), startOfISOWeek)(range.StartTime),
    EndTime: pipe(curryRight(subYears, yearsBack), curryRight(setISOWeek, getISOWeek(range.EndTime)), endOfISOWeek)(range.EndTime)
  };
};

module.exports = sameWeeksPreviousYear;