const mapValues = require('../../object/mapValues');
const formatDateTime = require('../formatDateTime');


const createDateRanges = (current, initial = current) => {
  const initialFormatted = mapValues(initial, rangeGroup =>
    mapValues(rangeGroup, formatDateTime));
  const currentFormatted = mapValues(current, rangeGroup =>
    mapValues(rangeGroup, formatDateTime));

  return {
    initial: initialFormatted,
    current: currentFormatted,
  };
};


module.exports = createDateRanges;
