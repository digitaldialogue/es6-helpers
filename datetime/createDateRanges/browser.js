"use strict";

var mapValues = require('../../object/mapValues');

var formatDateTime = require('../formatDateTime');

var createDateRanges = function createDateRanges(current) {
  var initial = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : current;
  var initialFormatted = mapValues(initial, function (rangeGroup) {
    return mapValues(rangeGroup, formatDateTime);
  });
  var currentFormatted = mapValues(current, function (rangeGroup) {
    return mapValues(rangeGroup, formatDateTime);
  });
  return {
    initial: initialFormatted,
    current: currentFormatted
  };
};

module.exports = createDateRanges;