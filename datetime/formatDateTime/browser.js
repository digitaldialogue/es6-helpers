"use strict";

var format = require('date-fns/format');

var datetimeFormat = 'YYYY-MM-DD HH:mm:ss.SSS';

var formatDateTime = function formatDateTime(datetime) {
  return format(datetime, datetimeFormat);
};

module.exports = formatDateTime;