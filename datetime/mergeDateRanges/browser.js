"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var parse = require('date-fns/parse');

var startOfDay = require('date-fns/start_of_day');

var endOfDay = require('date-fns/end_of_day');

var minuteInMs = 1000 * 60;

var rangesOverlap = function rangesOverlap(rOne, rTwo) {
  return rOne.StartTime >= rTwo.StartTime && parse(rOne.StartTime - minuteInMs) <= rTwo.EndTime || rTwo.StartTime >= rOne.StartTime && parse(rTwo.StartTime - minuteInMs) <= rOne.EndTime;
};

var getLargestRange = function getLargestRange(rOne, rTwo) {
  return {
    StartTime: startOfDay(parse(Math.min(rOne.StartTime, rTwo.StartTime))),
    EndTime: endOfDay(parse(Math.max(rOne.EndTime, rTwo.EndTime)))
  };
};
/**
 * Accepts an array of date ranges with StartTime & EndTime fields.
 * Will merge any date ranges that are overlapping or occur in sequence.
 * If date ranges supplied are more than 2, they must be ordered by StartTime ascending.
 */


module.exports = function (ranges) {
  return Object.values(ranges).reduce(function (arr, range) {
    return arr.length && rangesOverlap(range, arr[arr.length - 1]) ? [].concat(_toConsumableArray(arr.slice(0, arr.length - 1)), [getLargestRange(range, arr[arr.length - 1])]) : [].concat(_toConsumableArray(arr), [range]);
  }, []);
};