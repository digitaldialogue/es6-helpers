"use strict";

var format = require('date-fns/format');

var startOfISOWeek = require('date-fns/start_of_iso_week');

var startOfYesterday = require('date-fns/start_of_yesterday');

var isMonday = require('date-fns/is_monday');

var isTuesday = require('date-fns/is_tuesday');

var getWeekStart = function getWeekStart() {
  return format(startOfISOWeek(new Date()), 'ddd Do MMM');
};

var getYesterday = function getYesterday() {
  return format(startOfYesterday(), 'ddd Do MMM');
};

var getWeekToDate = function getWeekToDate() {
  return !isMonday(new Date()) && !isTuesday(new Date()) ? "".concat(getWeekStart(), " \u2013 ").concat(getYesterday()) : null;
};

module.exports = getWeekToDate;