const format = require('date-fns/format');
const startOfISOWeek = require('date-fns/start_of_iso_week');
const startOfYesterday = require('date-fns/start_of_yesterday');
const isMonday = require('date-fns/is_monday');
const isTuesday = require('date-fns/is_tuesday');

const getWeekStart = () => format(startOfISOWeek(new Date()), 'ddd Do MMM');
const getYesterday = () => format(startOfYesterday(), 'ddd Do MMM');


const getWeekToDate = () => !isMonday(new Date()) && !isTuesday(new Date())
  ? `${getWeekStart()} – ${getYesterday()}`
  : null;


module.exports = getWeekToDate;
