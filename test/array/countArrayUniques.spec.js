const { describe } = require('riteway');

const countArrayUniques = require('../../src/array/countArrayUniques');


describe('array.countArrayUniques()', async assert => {
  assert({
    given: '[] w/ 3 unique values',
    should: 'return {} w/ values & their count',
    expected: {
      hey: 4,
      hello: 2,
      please: 1,
    },
    actual: countArrayUniques(['hey', 'hello', 'hey', 'hey', 'please', 'hello', 'hey']),
  });
  assert({
    given: '[] w/ 1 unique value',
    should: 'return {} w/ value & its count',
    expected: {
      hola: 2,
    },
    actual: countArrayUniques(['hola', 'hola']),
  });
  assert({
    given: '[] w/ 5 unique values (strings & numbers)',
    should: 'return {} w/ values & their count',
    expected: {
      hola: 3,
      200: 1,
      100: 2,
      five: 2,
      x: 4,
    },
    actual: countArrayUniques(['x', 'x', 'hola', 100, 'hola', 'five', 'hola', 'x', 100, 200, 'x', 'five']),
  });
});
