const { describe } = require('riteway');

const flatten = require('../../src/array/flatten');


describe('array.flatten()', async assert => {
  assert({
    given: '[][][], no depth (default = 1)',
    should: 'return [][] w/ correct values',
    expected: [
      [1],
      2,
      3,
      4,
      [5],
      [6],
    ],
    actual: flatten([
      [[1], 2],
      [3, 4],
      [[5], [6]],
    ]),
  });
  assert({
    given: '[][][], depth of 2',
    should: 'return [] w/ correct values',
    expected: [1, 2, 3, 4, 5, 6],
    actual: flatten([
      [[1], 2],
      [3, 4],
      [[5], [6]],
    ], 2),
  });
  assert({
    given: '[][][][][], depth of 3',
    should: 'return [][] w/ correct values',
    expected: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    actual: flatten([
      [1],
      2,
      3,
      [4],
      5,
      [6, 7, [8]],
      9,
      10,
    ], 3),
  });
  assert({
    given: '[][][][][], depth of Infinity',
    should: 'return [] w/ correct values',
    expected: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    actual: flatten([
      [[[1]], [2], 3],
      [[[4], 5, [6, 7, [8]]]],
      [[9], [10]],
    ], Infinity),
  });
});
