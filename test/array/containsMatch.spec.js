const { describe } = require('riteway');

const containsMatch = require('../../src/array/containsMatch');


describe('array.containsMatch()', async assert => {
  // invalid
  assert({
    given: '{} [], valid key & invalid val',
    should: 'return false',
    expected: false,
    actual: containsMatch([{ label: 'mario' }], 'label', 'luigi'),
  });
  assert({
    given: '{} [], invalid key & valid val',
    should: 'return false',
    expected: false,
    actual: containsMatch([{ label: 'mario' }], 'id', 'mario'),
  });

  // valid
  assert({
    given: '{} [], valid key & val',
    should: 'return true',
    expected: true,
    actual: containsMatch([{ id: 1, label: 'mario' }, { id: 3, label: 'luigi' }], 'label', 'mario'),
  });

  // invalid w/ children
  assert({
    given: '{} w/ children [], valid key & invalid number val',
    should: 'return false',
    expected: false,
    actual: containsMatch([
      { id: 1, label: 'luigi', children: [{ id: 4, label: 'luigi jr.' }, { id: 5, label: 'bowser jr.' }] },
      { id: 2, label: 'mario' },
    ], 'id', 3),
  });
  assert({
    given: '{} w/ children [], valid key & invalid number val',
    should: 'return false',
    expected: false,
    actual: containsMatch([
      { id: 1, label: 'luigi', children: [] },
      { id: 2, label: 'mario', children: [{ id: 4, label: 'luigi jr.' }, { id: 5, label: 'bowser jr.' }] },
    ], 'label', 3),
  });
  assert({
    given: '{} w/ children [], invalid key & valid number val',
    should: 'return false',
    expected: false,
    actual: containsMatch([
      { id: 1, label: 'luigi', children: [{ id: 5, label: 'bowser jr.' }] },
      { id: 2, label: 'mario', children: [{ id: 4, label: 'luigi jr.' }] },
    ], 'i', 3),
  });

  // valid w/ children
  assert({
    given: '{}s w/ children [], valid key & val of child',
    should: 'return true',
    expected: true,
    actual: containsMatch([
      { id: 1, label: 'mario', children: [{ id: 3, label: 'mario jr.' }] },
      { id: 2, label: 'luigi', children: [{ id: 4, label: 'luigi jr.' }, { id: 5, label: 'bowser jr.' }] },
    ], 'label', 'mario jr.'),
  });
  assert({
    given: '{}s w/ children [], valid key & str val of child',
    should: 'return true',
    expected: true,
    actual: containsMatch([
      { id: 1, label: 'peach', children: [{ id: 2, label: 'mario jr.', children: [{ id: 4, label: 'mario jr. jr.' }] }] },
      { id: 3, label: 'luigi', children: [{ id: 5, label: 'luigi jr.' }, { id: 6, label: 'bowser jr.' }] },
    ], 'label', 'mario jr. jr.'),
  });
  assert({
    given: '{}s w/ children [], valid key & number val of child',
    should: 'return true',
    expected: true,
    actual: containsMatch([
      { id: 3, label: 'luigi', children: [{ id: 5, label: 'luigi jr.' }, { id: 6, label: 'bowser jr.' }] },
      { id: 1, label: 'peach', children: [{ id: 2, label: 'mario jr.', children: [{ id: 4, label: 'mario jr. jr.' }] }] },
    ], 'id', 4),
  });
});
