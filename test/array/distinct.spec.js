const { describe } = require('riteway');

const distinct = require('../../src/array/distinct');


describe('array.distinct()', async assert => {
  assert({
    given: '[] w/ 3 unique values',
    should: 'return [] w/ 3 values',
    expected: ['hey', 'hello', 'please'],
    actual: distinct(['hey', 'hello', 'hey', 'hey', 'please', 'hello', 'hey']),
  });
  assert({
    given: '[] w/ 1 unique value',
    should: 'return [] w/ value ',
    expected: ['hola'],
    actual: distinct(['hola', 'hola']),
  });
  assert({
    given: '[] w/ 5 unique values (strings & numbers)',
    should: 'return [] w/ values',
    expected: ['x', 'hola', 100, 'five', 200],
    actual: distinct(['x', 'x', 'hola', 100, 'hola', 'five', 'hola', 'x', 100, 200, 'x', 'five']),
  });
});
