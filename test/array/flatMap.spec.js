const { describe } = require('riteway');

const flatMap = require('../../src/array/flatMap');


describe('array.flatMap()', async assert => {
  assert({
    given: '[]-returning function, then [] w/ values',
    should: 'return flat [] w/ correct values',
    expected: [1, 2, 3, 4, 5, 6],
    actual: flatMap(no => [no, no + 1])([1, 3, 5]),
  });
  assert({
    given: '[]-returning function, then [] w/ values',
    should: 'return flat [] w/ correct values',
    expected: [
      { name: 'Not Drew' },
      { name: 'Drew' },
      { name: 'Not Andy' },
      { name: 'Andy' },
      { name: 'Not Andrew' },
      { name: 'Andrew' },
    ],
    actual: flatMap(obj =>[{ name: `Not ${obj.name}` }, obj])
      ([{ name: 'Drew' }, { name: 'Andy' }, { name: 'Andrew' }]),
  });
});
