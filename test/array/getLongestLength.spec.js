const { describe } = require('riteway');

const getLongestLength = require('../../src/array/getLongestLength');


describe('array.getLongestLength()', async assert => {
  assert({
    given: '[][] w/ empty []s',
    should: 'return 0',
    expected: 0,
    actual: getLongestLength([
      [],
      [],
    ]),
  });

  assert({
    given: '[][]',
    should: 'return length of longest []',
    expected: 4,
    actual: getLongestLength([
      [1, 2, 3],
      [4, 5, 6, 7],
    ]),
  });
  assert({
    given: '[][]',
    should: 'return length of longest []',
    expected: 7,
    actual: getLongestLength([
      [{ name: 'smth' }, { name: 'smth' }, 3],
      [{ name: 'smth' }, 2, 'three', { name: 'smth' }, { name: 'smth' }, 6, { name: 'smth' }],
      [{ name: 'smth' }, 2],
      [{ name: 'smth' }, 2, 'three', { name: 'smth' }],
    ]),
  });
});
