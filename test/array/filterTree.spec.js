const { describe } = require('riteway');

const filterTree = require('../../src/array/filterTree');


describe('array.filterTree()', async assert => {
  assert({
    given: 'tree, key & val that dont match any {}',
    should: 'return empty []',
    expected: [],
    actual: filterTree([
      { id: 1, label: 'DONT Gotem', children: [] },
      { id: 2, label: 'Gotem', children: [
        { id: 3, label: 'Gotem', children: [] },
        { id: 4, label: 'Something' }] },
    ], 'label', 'Ahoy'),
  });

  assert({
    given: 'tree, key & val',
    should: 'return tree [] w/ correct items & their children',
    expected: [{
      id: 2,
      label: 'Gotem',
      children: [{ id: 3, label: 'Gotem', children: [] }],
    }],
    actual: filterTree([
      { id: 1, label: 'DONT Gotem', children: [] },
      { id: 2, label: 'Gotem', children: [
        { id: 3, label: 'Gotem', children: [] },
        { id: 4, label: 'Something' }
      ]},
    ], 'label', 'Gotem'),
  });
  assert({
    given: 'tree, key & val',
    should: 'return tree [] w/ correct items & their children',
    expected: [
      { id: 1, label: 'Luigi Jr.', children: [
        { id: 5, label: 'Gotem', children: [{ id: 6, label: 'Luigi Jr.', children: [] }] },
        { id: 7, label: 'Luigi Jr.', children: [] },
      ] },
      { id: 2, label: 'Luigi', children: [{ id: 10, label: 'Luigi Jr.', children: [] }] }],
    actual: filterTree([
      { id: 1, label: 'Luigi Jr.', children: [
        { id: 5, label: 'Gotem', children: [{ id: 6, label: 'Luigi Jr.' }] },
        { id: 8, label: 'Bowser', children: [{ id: 9, label: 'Bowser Jr.' }] },
        { id: 7, label: 'Luigi Jr.' },
      ] },
      { id: 2, label: 'Luigi', children: [{ id: 10, label: 'Luigi Jr.', children: [{ label: 'Peach Jr.' }] }] },
    ], 'label', 'Luigi Jr.'),
  });
});
