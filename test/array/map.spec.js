const { describe } = require('riteway');

const map = require('../../src/array/map');


describe('array.map()', async assert => {
  assert({
    given: 'identity function & []',
    should: 'return [] w/ same values',
    expected: [
      { name: 'Andrew' },
      { name: 'Eric' },
      { name: 'Kris' },
    ],
    actual: map(x => x)([
      { name: 'Andrew' },
      { name: 'Eric' },
      { name: 'Kris' },
    ]),
  });

  assert({
    given: 'function & []',
    should: 'return [] w/ mapped values',
    expected: [1, 2, 3, 4],
    actual: map(x => x + 1)([0, 1, 2, 3]),
  });
  assert({
    given: 'function & []',
    should: 'return [] w/ mapped values',
    expected: [
      { name: 'Andrew', length: 6 },
      { name: 'Eric', length: 4 },
      { name: 'Kris', length: 4, role: 'CSS Master' },
    ],
    actual: map(x => ({
      ...x,
      length: x.name.length,
      ...(x.name === 'Kris' ? { role: 'CSS Master' }: {}),
    }))([
      { name: 'Andrew' },
      { name: 'Eric' },
      { name: 'Kris' },
    ]),
  });
});
