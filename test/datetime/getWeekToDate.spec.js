const { describe } = require('riteway');

const getWeekToDate = require('../../src/datetime/getWeekToDate');


describe('datetime.getWeekToDate()', async assert => {
  assert({
    given: 'a Monday date',
    should: 'return null',
    expected: null,
    actual: getWeekToDate(new Date('2019-06-03')),
  });
  assert({
    given: 'a Tuesday date',
    should: 'return null',
    expected: null,
    actual: getWeekToDate(new Date('2019-06-04')),
  });
  assert({
    given: 'a Wednesday date',
    should: 'return correct date range string',
    expected: 'Mon 3rd Jun – Tue 4th Jun',
    actual: getWeekToDate(new Date('2019-06-05')),
  });
  assert({
    given: 'a Thursday date',
    should: 'return correct date range string',
    expected: 'Mon 3rd Jun – Wed 5th Jun',
    actual: getWeekToDate(new Date('2019-06-06')),
  });
  assert({
    given: 'a Friday date',
    should: 'return correct date range string',
    expected: 'Mon 3rd Jun – Thu 6th Jun',
    actual: getWeekToDate(new Date('2019-06-07')),
  });
  assert({
    given: 'a Saturday date',
    should: 'return correct date range string',
    expected: 'Mon 3rd Jun – Fri 7th Jun',
    actual: getWeekToDate(new Date('2019-06-08')),
  });
  assert({
    given: 'a Sunday date',
    should: 'return correct date range string',
    expected: 'Mon 3rd Jun – Sat 8th Jun',
    actual: getWeekToDate(new Date('2019-06-09')),
  });
});
