const { describe } = require('riteway');

const formatDateTime = require('../../src/datetime/formatDateTime');


describe('datetime.formatDateTime()', async assert => {
  assert({
    given: 'a native date w/o hours',
    should: 'return correct formatted datetime string in UTC (+11)',
    expected: '2019-01-01 11:00:00.000',
    actual: formatDateTime(new Date('2019-01-01')),
  });
  assert({
    given: 'a native date w/ hours',
    should: 'return correct formatted datetime string',
    expected: '2019-09-21 14:31:00.000',
    actual: formatDateTime(new Date('2019-09-21 14:31')),
  });
  assert({
    given: 'a native date w/ hours',
    should: 'return correct formatted datetime string',
    expected: '2018-09-10 02:00:15.101',
    actual: formatDateTime(new Date('2018-09-10 2:00:15.101')),
  });
});
