const { describe } = require('riteway');

const createDateRanges = require('../../src/datetime/createDateRanges');


describe('datetime.createDateRanges()', async assert => {
  assert({
    given: '{} w/ current & initial dateRanges',
    should: 'return {} w/ same structure & formatted dates',
    expected: {
      current: {
        Another: { date: '2019-04-21 14:30:00.000', another: '2019-01-11 18:35:35.555' },
      },
      initial: {
        Something: { datum: '2019-04-15 23:23:23.333' },
      },
    },
    actual: createDateRanges({
      Another: { date: new Date('2019-04-21 14:30'), another: new Date('2019-01-11 18:35:35.555') },
    }, {
      Something: { datum: new Date('2019-04-15 23:23:23.333') },
    }),
  });
  assert({
    given: '{} w/ current & initial dateRanges',
    should: 'return {} w/ same structure & formatted dates',
    expected: {
      current: {
        Primary: { date: '2019-01-01 01:00:00.000', another: '2019-01-10 22:59:59.000' },
      },
      initial: {
        Non: { start: '2019-01-01 00:00:00.000', end: '2019-01-30 12:50:51.501' },
        Something: { daytona: '2019-02-01 00:05:00.000', datum: '2019-02-28 23:59:00.000' },
        AnotherThing: { startDate: '2019-02-01 00:00:00.000', endDate: '2019-02-10 23:59:59.000' },
      },
    },
    actual: createDateRanges({
      Primary: { date: new Date('2019-01-01 1:00'), another: new Date('2019-01-10 22:59:59') },
    }, {
      Non: { start: new Date('2019-01-01 00:00'), end: new Date('2019-01-30 12:50:51.501') },
      Something: { daytona: new Date('2019-02-01 00:05'), datum: new Date('2019-02-28 23:59') },
      AnotherThing: { startDate: new Date('2019-02-01 00:00'), endDate: new Date('2019-02-10 23:59:59') },
    }),
  });
});
