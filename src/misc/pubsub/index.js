const publishEvent = (event, data) =>
  window.dispatchEvent(new CustomEvent(event, data));

const subscribeToEvent = (event, func, options) =>
  window.addEventListener(event, func, options);

module.exports = {
  publishEvent,
  subscribeToEvent,
};
