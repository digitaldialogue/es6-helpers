const pipe = require('../../functional/pipe');
const camelCase = require('../../string/camelCase');


const lineIsAComment = (isComment = true) => line => line.startsWith('--') === isComment;
const toNumberOrString = val => Number.isNaN(Number(val)) ? val : Number(val);
const alphaNumericOnly = val => val.replace(/[^\d\w]/g, '');
const trim = val => val.trim();

const getCleanVal = pipe(
  trim,
  toNumberOrString,
);
const getCleanKey = pipe(
  alphaNumericOnly,
  camelCase,
);
const getObject = keys => (line, index) => ({
  lineNo: index + 1,
  ...line
    .trim()
    .split(',')
    .reduce((obj, val, i) => ({ ...obj, [keys[i]]: getCleanVal(val) }), {}),
});

const getObjects = (fileStr, keys, hasHeaders = false) => fileStr
  .trim()
  .split('\n')
  .slice(hasHeaders ? 1 : 0)
  .filter(lineIsAComment(false))
  .map(getObject(keys));

const getMetadata = fileStr => fileStr
  .trim()
  .split('\n')
  .filter(lineIsAComment())
  .map(line => line
    .trim()
    .split(': '))
  .reduce((obj, [key, val]) => ({ ...obj, [getCleanKey(key)]: val }), {});

const isFileEmpty = fileStr => fileStr.trim() === '';


module.exports = {
  isFileEmpty,
  getObjects,
  getMetadata,
};
