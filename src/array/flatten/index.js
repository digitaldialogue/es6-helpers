const pipe = require('../../functional/pipe');

const flatten = (arr, maxDepth = 1, currDepth = 1) => pipe(
  currArr => currArr.reduce((newArr, x) => [...newArr, ...(x.map ? x : [x])], []),
  newArr => currDepth === maxDepth || arr.every(x => !x.map)
    ? newArr
    : flatten(newArr, maxDepth, currDepth + 1),
)(arr);

module.exports = flatten;
