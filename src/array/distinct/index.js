const distinct = arr => [...new Set(arr)];

module.exports = distinct;
