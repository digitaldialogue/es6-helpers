const containsMatch = require('../containsMatch');

const filterTree = (arr = [], key, value) => arr
  .filter(item => containsMatch([item], key, value))
  .map(item => ({ ...item, children: filterTree(item.children, key, value) }));

module.exports = filterTree;
