const getLength = arr => arr.length;

module.exports = arrays => Math.max(...arrays.map(getLength));
