const toLowerCaseStr = val => String(val).toLowerCase();
const isMatch = (itemValue, searchValue) => toLowerCaseStr(itemValue) === toLowerCaseStr(searchValue);

const containsMatch = (arr, key, value) =>
  arr.some(item => isMatch(item[key], value)
    || (item.children && containsMatch(item.children, key, value)));

module.exports = containsMatch;
