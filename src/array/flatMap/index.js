const flatMap = mapFn => arr => arr.reduce((newArr, cur, index) =>
  [...newArr, ...mapFn(cur, index)], []);

module.exports = flatMap;
