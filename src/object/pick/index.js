const pick = (obj, keys) => keys.reduce((newObj, key) => obj[key]
  ? ({ ...newObj, [key]: obj[key] })
  : ({ ...newObj }), {})

pick.one = key => obj => obj[key];


module.exports = pick;
