const isObject = obj => Object.prototype.toString.call(obj).slice(8, -1) === 'Object';

const areObjects = (objs, fnName = 'every') => objs[fnName](isObject);

const allKeysMatch = (oOne, oTwo) => Object.keys(oOne).every(key => Object.keys(oTwo).includes(key));

const callbackIfObject = (oOne, oTwo, callback) =>
  areObjects([oOne, oTwo], 'some')
    ? callback(oOne, oTwo)
    : true;

const keyNotOpt = key => !key.startsWith('_');
const hasOpt = (obj, opt) => obj[`_${opt}`];

const validateObjKeys = (obj, schema) => hasOpt(schema, 'allowAdditionalKeys') || allKeysMatch(obj, schema);

const validateObjWithNesting = (obj, schema, callback) => schemaKey => Object.keys(obj).includes(schemaKey)
  && callbackIfObject(obj[schemaKey], schema[schemaKey], callback);

const validate = (obj, schema) => areObjects([obj, schema])
  && (hasOpt(schema, 'skipKeys')
    || (validateObjKeys(obj, schema)
      && Object.keys(schema)
        .filter(keyNotOpt)
        .every(validateObjWithNesting(obj, schema, validate))));

/**
 * Iterates over each key in the object to ensure the model object has the same keys.
 * Will iterate through any other nested objects accordingly.
 */
module.exports = validate;
