const parse = require('date-fns/parse');
const startOfDay = require('date-fns/start_of_day');
const endOfDay = require('date-fns/end_of_day');

const minuteInMs = 1000 * 60;

const rangesOverlap = (rOne, rTwo) =>
  (rOne.StartTime >= rTwo.StartTime
    && parse(rOne.StartTime - minuteInMs) <= rTwo.EndTime)
  || (rTwo.StartTime >= rOne.StartTime
    && parse(rTwo.StartTime - minuteInMs) <= rOne.EndTime);

const getLargestRange = (rOne, rTwo) => ({
  StartTime: startOfDay(parse(Math.min(rOne.StartTime, rTwo.StartTime))),
  EndTime: endOfDay(parse(Math.max(rOne.EndTime, rTwo.EndTime))),
});

/**
 * Accepts an array of date ranges with StartTime & EndTime fields.
 * Will merge any date ranges that are overlapping or occur in sequence.
 * If date ranges supplied are more than 2, they must be ordered by StartTime ascending.
 */
module.exports = ranges =>
  Object.values(ranges).reduce((arr, range) =>
    (arr.length && rangesOverlap(range, arr[arr.length - 1])
      ? [...arr.slice(0, arr.length - 1), getLargestRange(range, arr[arr.length - 1])]
      : [...arr, range]), []);
