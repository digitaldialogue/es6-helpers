const format = require('date-fns/format');
const startOfISOWeek = require('date-fns/start_of_iso_week');
const subDays = require('date-fns/sub_days');
const isMonday = require('date-fns/is_monday');
const isTuesday = require('date-fns/is_tuesday');

const getWeekStart = today => format(startOfISOWeek(today), 'ddd Do MMM');
const getYesterday = today => format(subDays(today, 1), 'ddd Do MMM');


const getWeekToDate = (today = new Date()) => !isMonday(today) && !isTuesday(today)
  ? `${getWeekStart(today)} – ${getYesterday(today)}`
  : null;


module.exports = getWeekToDate;
