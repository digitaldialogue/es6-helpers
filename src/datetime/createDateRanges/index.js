const mapValues = require('../../object/mapValues');
const formatDateTime = require('../formatDateTime');


const formatGroup = group => mapValues(group, rangeGroup =>
  mapValues(rangeGroup, formatDateTime));

const createDateRanges = (current, initial = current) => ({
  initial: formatGroup(initial),
  current: formatGroup(current),
});


module.exports = createDateRanges;
