const format = require('date-fns/format');

const datetimeFormat = 'YYYY-MM-DD HH:mm:ss.SSS';

const formatDateTime = datetime => format(datetime, datetimeFormat);

module.exports = formatDateTime;
