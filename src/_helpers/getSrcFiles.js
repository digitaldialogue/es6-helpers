const glob = require('glob');


const getSrcFiles = (srcDir = '../src') => () => new Promise(resolve =>
  glob(`${srcDir}/**/*.js`, { ignore: `${srcDir}/_*/**` }, (_err, files) => resolve(files)));


module.exports = getSrcFiles;
