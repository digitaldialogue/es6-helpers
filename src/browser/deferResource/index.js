const deferResource = (type, obj) => document.head.appendChild(Object.assign(
  document.createElement(type),
  obj,
));

module.exports = deferResource;
