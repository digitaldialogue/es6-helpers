module.exports = {
  get(key, fallback) {
    const value = sessionStorage.getItem(key);
    return value === null ? fallback : JSON.parse(value);
  },

  set: (key, value) => sessionStorage.setItem(key, JSON.stringify(value)),

  remove: key => sessionStorage.removeItem(key),

  clearAll: () => sessionStorage.clear(),
};
