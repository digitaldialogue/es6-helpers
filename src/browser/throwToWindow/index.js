const throwToWindow = err => setTimeout(() => { throw err; });

module.exports = throwToWindow;
