const Lockr = require('lockr');

Lockr.prefix = 'blix.';

const setData = data => Object.keys(data).forEach(key => Lockr.set(key, data[key]));

module.exports = {
  clearAll(except = []) {
    const exceptedItems = except.reduce((obj, key) => Object.assign({ [key]: Lockr.get(key) }, obj), {});
    Lockr.flush();
    setData(exceptedItems);
  },

  // Note: Lockr.rm doesn't work for some reason unfortunately
  remove: key => Lockr.set(key, null),

  get(key, fallback) {
    return Lockr.get(key) || fallback;
  },

  getData() {
    return Lockr.getAll(true).reduce((obj, kv) => ({ ...obj, ...kv }), {});
  },

  set(key, value) {
    return Lockr.set(key, value);
  },

  setData,
};
