const getViewportWidth = () => Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

module.exports = getViewportWidth;
