module.exports = (fn, ...params) => x => fn(x, ...params);
