const passthroughAsync = (func, passParams = true) => async (...params) => {
  if (passParams) await func(...params);
  else await func();
  return params.length > 1
    ? params
    : params[0];
};


module.exports = passthroughAsync;
