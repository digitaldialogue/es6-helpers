const pipeWith = pipeFn => (...fns) => fns.reduce((res, fn) => (...x) => pipeFn(fn, res(...x)));

module.exports = pipeWith;
