const parallel = (...fns) => (...params) =>
  Promise.all(fns.map(f => f(...params)));

module.exports = parallel;
