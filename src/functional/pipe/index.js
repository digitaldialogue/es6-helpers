const pipe = (res, fn) => (...arg) => fn(res(...arg));

module.exports = (...ops) => ops.reduce(pipe);
