const camelCase = str => str
  .replace(/(?:^\w|[A-Z]|\b\w|_\w)/g, (letter, index) =>
    index === 0 ? letter.toLowerCase() : letter.toUpperCase())
  .replace(/(\s|-|_)+/g, '');


module.exports = camelCase;
