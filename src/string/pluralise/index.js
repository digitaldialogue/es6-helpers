module.exports = (number, noun) => (number === 1 ? `1 ${noun}` : `${number} ${noun}s`);
