/* eslint-disable no-bitwise */

module.exports = (str) => {
  let hash = 0;
  if (str.length === 0) return hash;

  Array.from(str).forEach((char, i) => {
    const chr = str.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0;
  });

  return hash;
};
