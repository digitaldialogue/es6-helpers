const fs = require('fs');

const pipeAsync = require('./src/functional/pipeAsync');
const getSrcFiles = require('./src/_helpers/getSrcFiles');

const currentPJ = require('./package.json');


const getCompiledPaths = srcFiles => srcFiles.map(f => f.replace(/\/src/, ''))

const getPathMappings = compiledPaths => compiledPaths.reduce((obj, indexPath) => ({
  ...obj,
  [indexPath]: indexPath.replace(/index.js/, 'browser.js'),
}), {});

const getUpdatedPJ = currentPJ => pathMappings => ({
  ...currentPJ,
  browser: { ...pathMappings },
});

const setPJ = pJ => new Promise((resolve, reject) =>
  fs.writeFile('./package.json', JSON.stringify(pJ, null, 2), err => err ? reject(err) : resolve()));


const updatePJ = pipeAsync(
  getSrcFiles('./src'),
  getCompiledPaths,
  getPathMappings,
  getUpdatedPJ(currentPJ),
  setPJ,
);

const run = async () => {
  await updatePJ()
  console.log('package.json updated successfully!');
};

run().catch(console.error);
