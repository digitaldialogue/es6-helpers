const groupBy = (iterable, fn) => Object.values(iterable).reduce((obj, val) => {
  const groupKey = fn(val);

  return {
    ...obj,
    [groupKey]: obj[groupKey]
      ? [...obj[groupKey], val]
      : [val],
  };
}, {});


module.exports = groupBy;
