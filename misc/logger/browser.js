"use strict";

var pipe = require('../../functional/pipe');

var level = process.env.LOGGING_LEVEL ? Number(process.env.LOGGING_LEVEL) || 0 : 3;

var shouldLog = function shouldLog(levelRequired) {
  return (!process.env.LOGGING || process.env.LOGGING === 'ENABLE') && level >= levelRequired;
};

var colours = {
  // https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color#41407246
  red: '\x1b[31m',
  yellow: '\x1b[33m',
  green: '\x1b[32m',
  blue: '\x1b[34m',
  magenta: '\x1b[35m',
  cyan: '\x1b[36m',
  reset: '\x1b[0m',
  white: '\x1b[37m'
};

var getColouredText = function getColouredText(defaultColour) {
  return function (text, overrideColour) {
    return !process.env.LOGGING_COLOUR || process.env.LOGGING_COLOUR === 'ENABLE' ? "".concat(colours[overrideColour] || colours[defaultColour]).concat(text).concat(colours.reset) : text;
  };
};

var writeToConsole = function writeToConsole(fnName) {
  return function (text) {
    return console[fnName](text);
  };
};

var createLogFn = function createLogFn() {
  var defaultColour = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'white';
  var consoleFnName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'log';
  var levelRequired = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3;
  return shouldLog(levelRequired) && pipe(getColouredText(defaultColour), writeToConsole(consoleFnName));
};

var logger = {
  log: createLogFn(),
  dir: createLogFn('cyan', 'dir'),
  info: createLogFn('cyan', 'info'),
  success: createLogFn('green', 'log', 2),
  warn: createLogFn('yellow', 'warn', 1),
  error: createLogFn('red', 'error', 0)
};
module.exports = logger;