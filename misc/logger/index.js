const pipe = require('../../functional/pipe');


const level = process.env.LOGGING_LEVEL
  ? Number(process.env.LOGGING_LEVEL) || 0
  : 3;
const shouldLog = levelRequired =>
  (!process.env.LOGGING || process.env.LOGGING === 'ENABLE')
  && level >= levelRequired
const colours = {
  // https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color#41407246
  red: '\x1b[31m',
  yellow: '\x1b[33m',
  green: '\x1b[32m',
  blue: '\x1b[34m',
  magenta: '\x1b[35m',
  cyan: '\x1b[36m',
  reset: '\x1b[0m',
  white: '\x1b[37m',
};

const getColouredText = defaultColour => (text, overrideColour) =>
  !process.env.LOGGING_COLOUR || process.env.LOGGING_COLOUR === 'ENABLE'
    ? `${colours[overrideColour] || colours[defaultColour]}${text}${colours.reset}`
    : text;
const writeToConsole = fnName => text => console[fnName](text);
const createLogFn = (defaultColour = 'white', consoleFnName = 'log', levelRequired = 3) =>
  shouldLog(levelRequired) && pipe(
    getColouredText(defaultColour),
    writeToConsole(consoleFnName),
  );


const logger = {
  log: createLogFn(),
  dir: createLogFn('cyan', 'dir'),
  info: createLogFn('cyan', 'info'),
  success: createLogFn('green', 'log', 2),
  warn: createLogFn('yellow', 'warn', 1),
  error: createLogFn('red', 'error', 0),
};

module.exports = logger;
