"use strict";

var publishEvent = function publishEvent(event, data) {
  return window.dispatchEvent(new CustomEvent(event, data));
};

var subscribeToEvent = function subscribeToEvent(event, func, options) {
  return window.addEventListener(event, func, options);
};

module.exports = {
  publishEvent: publishEvent,
  subscribeToEvent: subscribeToEvent
};