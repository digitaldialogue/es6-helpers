"use strict";

var regex = {
  dateTimeFullISO8601: new RegExp(/^[12]\d{3}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])T((\d|[01]\d|2[0-3]):[0-5]\d|24:00)(:([0-5]\d))?(.\d{1,3})?(\+\d{2})?(:\d{2})?$/),
  dateTime: new RegExp(/^[12]\d{3}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01]) ((\d|[01]\d|2[0-3]):[0-5]\d|24:00)$/),
  date: new RegExp(/^[12]\d{3}-(0[1-9]|1[012])-(0[1-9]|[12]\d|3[01])$/),
  time: new RegExp(/((\d|[01]\d|2[0-3]):[0-5]\d|24:00)$/),
  number: new RegExp(/^-?\d+$/),
  decimal: new RegExp(/^-?\d*\.?\d+$/)
};

var satisfiesRegex = function satisfiesRegex(exp) {
  return function (val) {
    return !!exp.exec(val);
  };
};

var isString = function isString(val) {
  return typeof val === 'string';
};

var isType = {
  string: isString,
  number: satisfiesRegex(regex.number),

  /**
   * Datetime must satisfy the full ISO8601 datetime format YYYY-MM-DDThh (with optional) :mm:ss.sss+hh:mm
   */
  dateTimeFullISO8601: function dateTimeFullISO8601(val) {
    return isString(val) && satisfiesRegex(regex.dateTimeFullISO8601)(val);
  },

  /**
   * Datetime must satisfy format YYYY-MM-DD hh:mm
   */
  dateTime: function dateTime(val) {
    return isString(val) && satisfiesRegex(regex.dateTime)(val);
  },

  /**
   * Date must satisfy format YYYY-MM-DD
   */
  date: satisfiesRegex(regex.date),

  /**
   * Time must satisfy format hh:mm
   */
  time: satisfiesRegex(regex.time),
  decimal: satisfiesRegex(regex.decimal)
};
module.exports = isType;