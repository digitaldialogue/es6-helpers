"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var pipe = require('../../functional/pipe');

var camelCase = require('../../string/camelCase');

var lineIsAComment = function lineIsAComment() {
  var isComment = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
  return function (line) {
    return line.startsWith('--') === isComment;
  };
};

var toNumberOrString = function toNumberOrString(val) {
  return Number.isNaN(Number(val)) ? val : Number(val);
};

var alphaNumericOnly = function alphaNumericOnly(val) {
  return val.replace(/[^\d\w]/g, '');
};

var trim = function trim(val) {
  return val.trim();
};

var getCleanVal = pipe(trim, toNumberOrString);
var getCleanKey = pipe(alphaNumericOnly, camelCase);

var getObject = function getObject(keys) {
  return function (line, index) {
    return _objectSpread({
      lineNo: index + 1
    }, line.trim().split(',').reduce(function (obj, val, i) {
      return _objectSpread({}, obj, _defineProperty({}, keys[i], getCleanVal(val)));
    }, {}));
  };
};

var getObjects = function getObjects(fileStr, keys) {
  var hasHeaders = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  return fileStr.trim().split('\n').slice(hasHeaders ? 1 : 0).filter(lineIsAComment(false)).map(getObject(keys));
};

var getMetadata = function getMetadata(fileStr) {
  return fileStr.trim().split('\n').filter(lineIsAComment()).map(function (line) {
    return line.trim().split(': ');
  }).reduce(function (obj, _ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        val = _ref2[1];

    return _objectSpread({}, obj, _defineProperty({}, getCleanKey(key), val));
  }, {});
};

var isFileEmpty = function isFileEmpty(fileStr) {
  return fileStr.trim() === '';
};

module.exports = {
  isFileEmpty: isFileEmpty,
  getObjects: getObjects,
  getMetadata: getMetadata
};