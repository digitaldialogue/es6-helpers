"use strict";

module.exports = function (fn) {
  var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 250;
  // SRC: https://github.com/tuxsudo/es6y-throttle
  var timer = null;

  function throttledFn() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    if (!timer) {
      timer = setTimeout(function () {
        fn.apply(void 0, args);
        timer = null;
      }, time);
    }
  }

  throttledFn.cancel = function () {
    clearTimeout(timer);
    timer = null;
  };

  return throttledFn();
};