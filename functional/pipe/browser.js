"use strict";

var pipe = function pipe(res, fn) {
  return function () {
    return fn(res.apply(void 0, arguments));
  };
};

module.exports = function () {
  for (var _len = arguments.length, ops = new Array(_len), _key = 0; _key < _len; _key++) {
    ops[_key] = arguments[_key];
  }

  return ops.reduce(pipe);
};