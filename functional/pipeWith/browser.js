"use strict";

var pipeWith = function pipeWith(pipeFn) {
  return function () {
    for (var _len = arguments.length, fns = new Array(_len), _key = 0; _key < _len; _key++) {
      fns[_key] = arguments[_key];
    }

    return fns.reduce(function (res, fn) {
      return function () {
        return pipeFn(fn, res.apply(void 0, arguments));
      };
    });
  };
};

module.exports = pipeWith;