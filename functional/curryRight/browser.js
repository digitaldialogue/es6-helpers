"use strict";

module.exports = function (fn) {
  for (var _len = arguments.length, params = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    params[_key - 1] = arguments[_key];
  }

  return function (x) {
    return fn.apply(void 0, [x].concat(params));
  };
};