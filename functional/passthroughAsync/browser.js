"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var passthroughAsync = function passthroughAsync(func) {
  var passParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  return (
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var _len,
          params,
          _key,
          _args = arguments;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              for (_len = _args.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
                params[_key] = _args[_key];
              }

              if (!passParams) {
                _context.next = 6;
                break;
              }

              _context.next = 4;
              return func.apply(void 0, params);

            case 4:
              _context.next = 8;
              break;

            case 6:
              _context.next = 8;
              return func();

            case 8:
              return _context.abrupt("return", params.length > 1 ? params : params[0]);

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))
  );
};

module.exports = passthroughAsync;