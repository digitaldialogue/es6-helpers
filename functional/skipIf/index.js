const skipIf = (condition, fn) => condition ? x => x : fn;

module.exports = skipIf;
