"use strict";

var skipIf = function skipIf(condition, fn) {
  return condition ? function (x) {
    return x;
  } : fn;
};

module.exports = skipIf;