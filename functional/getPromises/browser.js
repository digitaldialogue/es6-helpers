"use strict";

/**
 * Gets you a bunch of promises (Promise.all) from your array using a mapping function you provide.
 * Useful for converting a bunch of things into promises and awaiting them all.
 * (Curried: mapping function => iterable) 
 * @param {*} mappingFn 
 */
var getPromises = function getPromises(mappingFn) {
  return function (arr) {
    return Promise.all(arr.map(mappingFn));
  };
};

module.exports = getPromises;