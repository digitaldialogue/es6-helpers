const pipe = (...fns) => x => fns.reduce(async (res, fn) => fn(await res), x);

module.exports = pipe;
