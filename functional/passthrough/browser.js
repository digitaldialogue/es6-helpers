"use strict";

// Takes a func then later params to pass to said func when executing
// the catch: we don't return the result of the func executed,
// but we return the params passed - as is - back.
var passthrough = function passthrough(func) {
  var passParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  return function () {
    for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
      params[_key] = arguments[_key];
    }

    if (passParams) func.apply(void 0, params);else func();
    return params.length > 1 ? params : params[0];
  };
};

module.exports = passthrough;