// Takes a func then later params to pass to said func when executing
// the catch: we don't return the result of the func executed,
// but we return the params passed - as is - back.
const passthrough = (func, passParams = true) => (...params) => {
  if (passParams) func(...params);
  else func();
  return params.length > 1
    ? params
    : params[0];
};


module.exports = passthrough;
