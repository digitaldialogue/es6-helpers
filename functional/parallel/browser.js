"use strict";

var parallel = function parallel() {
  for (var _len = arguments.length, fns = new Array(_len), _key = 0; _key < _len; _key++) {
    fns[_key] = arguments[_key];
  }

  return function () {
    for (var _len2 = arguments.length, params = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      params[_key2] = arguments[_key2];
    }

    return Promise.all(fns.map(function (f) {
      return f.apply(void 0, params);
    }));
  };
};

module.exports = parallel;