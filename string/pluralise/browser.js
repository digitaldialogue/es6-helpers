"use strict";

module.exports = function (number, noun) {
  return number === 1 ? "1 ".concat(noun) : "".concat(number, " ").concat(noun, "s");
};