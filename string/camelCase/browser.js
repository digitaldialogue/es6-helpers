"use strict";

var camelCase = function camelCase(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w|_\w)/g, function (letter, index) {
    return index === 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/(\s|-|_)+/g, '');
};

module.exports = camelCase;