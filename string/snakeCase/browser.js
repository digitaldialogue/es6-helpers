"use strict";

module.exports = function (string) {
  return string.toLowerCase().replace(/ /g, '_');
};