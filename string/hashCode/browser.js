"use strict";

/* eslint-disable no-bitwise */
module.exports = function (str) {
  var hash = 0;
  if (str.length === 0) return hash;
  Array.from(str).forEach(function (char, i) {
    var chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0;
  });
  return hash;
};