"use strict";

var upperCaseFirstLetter = function upperCaseFirstLetter(str) {
  return str.split('').map(function (letter, index) {
    return index === 0 ? letter.toUpperCase() : letter;
  }).join('');
};

var uncamelCase = function uncamelCase(camelStr) {
  var shouldUpperCaseFirstLetter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return camelStr.match(new RegExp(/([A-Za-z][a-z]*)/g)).map(shouldUpperCaseFirstLetter ? upperCaseFirstLetter : function (word) {
    return word.toLowerCase();
  }).join(' ');
};

module.exports = uncamelCase;