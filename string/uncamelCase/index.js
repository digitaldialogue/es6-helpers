const upperCaseFirstLetter = str => str
  .split('')
  .map((letter, index) => index === 0 ? letter.toUpperCase() : letter)
  .join('');

const uncamelCase = (camelStr, shouldUpperCaseFirstLetter = false) => camelStr
  .match(new RegExp(/([A-Za-z][a-z]*)/g))
  .map(shouldUpperCaseFirstLetter ? upperCaseFirstLetter : word => word.toLowerCase())
  .join(' ');


module.exports = uncamelCase;
