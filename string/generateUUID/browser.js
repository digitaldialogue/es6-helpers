"use strict";

var uuidTemplate = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';

var generateUUID = function generateUUID() {
  var d = new Date().getTime();
  return uuidTemplate.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0; // eslint-disable-line no-bitwise

    d = Math.floor(d / 16);
    return (c === 'x' ? r : r & 0x7 | 0x8).toString(16); // eslint-disable-line no-bitwise
  });
};

module.exports = generateUUID;