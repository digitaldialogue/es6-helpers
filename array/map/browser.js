"use strict";

var map = function map(fn) {
  return function (arr) {
    return arr.map(fn);
  };
};

module.exports = map;