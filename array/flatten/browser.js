"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var pipe = require('../../functional/pipe');

var flatten = function flatten(arr) {
  var maxDepth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var currDepth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  return pipe(function (currArr) {
    return currArr.reduce(function (newArr, x) {
      return [].concat(_toConsumableArray(newArr), _toConsumableArray(x.map ? x : [x]));
    }, []);
  }, function (newArr) {
    return currDepth === maxDepth || arr.every(function (x) {
      return !x.map;
    }) ? newArr : flatten(newArr, maxDepth, currDepth + 1);
  })(arr);
};

module.exports = flatten;