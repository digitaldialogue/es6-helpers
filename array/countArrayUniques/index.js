module.exports = arr => arr.reduce((obj, val) => ({
  ...obj,
  [val]: val in obj ? obj[val] + 1 : 1
}), {});
