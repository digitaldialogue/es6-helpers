"use strict";

var toLowerCaseStr = function toLowerCaseStr(val) {
  return String(val).toLowerCase();
};

var isMatch = function isMatch(itemValue, searchValue) {
  return toLowerCaseStr(itemValue) === toLowerCaseStr(searchValue);
};

var containsMatch = function containsMatch(arr, key, value) {
  return arr.some(function (item) {
    return isMatch(item[key], value) || item.children && containsMatch(item.children, key, value);
  });
};

module.exports = containsMatch;