"use strict";

/**
 * Run Promises in sequence. This is the equivalent of chaining .then() calls
 * but deals with the issue of having a variable or unknown number of Promises
 * being returned. Since Promises run as soon as they are defined, they need to
 * be wrapped in a function.
 */
module.exports = function (functions) {
  return functions.reduce(function (promiseChain, func) {
    return promiseChain.then(func);
  }, Promise.resolve());
};