const { ncp } = require('ncp');
const { transformFileAsync } = require('@babel/core');

const fs = require('fs');

const pipeAsync = require('./src/functional/pipeAsync');
const getSrcFiles = require('./src/_helpers/getSrcFiles');

const SRC_DIR = './src';


const getCompiledFile = (srcDir = SRC_DIR) => async srcPath => ({
  path: srcPath
    .replace(srcDir, '.')
    .replace(/index/, 'browser'),
  code: (await transformFileAsync(srcPath)).code,
});

const compileFiles = srcFiles => Promise.all(srcFiles.map(getCompiledFile()));

const writeFile = ({ path, code }) => new Promise(resolve =>
  fs.writeFile(path, code, _err => resolve()));

const writeCompiledFiles = compiledFiles => Promise.all(compiledFiles.map(writeFile));


const copySrc = (srcDir = SRC_DIR) => new Promise((resolve, reject) =>
  ncp(srcDir, './', { filter: name => !name.includes('_') }, err => err ? reject(err) : resolve()));

const buildBrowser = pipeAsync(
  getSrcFiles(SRC_DIR),
  compileFiles,
  writeCompiledFiles,
);

const run = async () => {
  await copySrc();
  await buildBrowser();
  console.info('Build finished successfully!');
};

run().catch(console.error);
