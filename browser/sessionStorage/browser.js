"use strict";

module.exports = {
  get: function get(key, fallback) {
    var value = sessionStorage.getItem(key);
    return value === null ? fallback : JSON.parse(value);
  },
  set: function set(key, value) {
    return sessionStorage.setItem(key, JSON.stringify(value));
  },
  remove: function remove(key) {
    return sessionStorage.removeItem(key);
  },
  clearAll: function clearAll() {
    return sessionStorage.clear();
  }
};