"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Lockr = require('lockr');

Lockr.prefix = 'blix.';

var setData = function setData(data) {
  return Object.keys(data).forEach(function (key) {
    return Lockr.set(key, data[key]);
  });
};

module.exports = {
  clearAll: function clearAll() {
    var except = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var exceptedItems = except.reduce(function (obj, key) {
      return _extends(_defineProperty({}, key, Lockr.get(key)), obj);
    }, {});
    Lockr.flush();
    setData(exceptedItems);
  },
  // Note: Lockr.rm doesn't work for some reason unfortunately
  remove: function remove(key) {
    return Lockr.set(key, null);
  },
  get: function get(key, fallback) {
    return Lockr.get(key) || fallback;
  },
  getData: function getData() {
    return Lockr.getAll(true).reduce(function (obj, kv) {
      return _objectSpread({}, obj, kv);
    }, {});
  },
  set: function set(key, value) {
    return Lockr.set(key, value);
  },
  setData: setData
};