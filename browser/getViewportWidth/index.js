module.exports = () => Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
