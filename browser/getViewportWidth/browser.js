"use strict";

module.exports = function () {
  return Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
};