"use strict";

var queryString = require('../queryString');

var mapValues = require('../../object/mapValues');

var objectsAreEqual = require('../../object/areEqual');

var toNumbers = function toNumbers(obj) {
  return mapValues(obj, function (val) {
    return Number.isNaN(Number(val)) ? val : Number(val);
  });
};

var isDestinationMatchingCurrent = function isDestinationMatchingCurrent(dest) {
  var curr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : window.location;
  return curr.origin === dest.origin && curr.pathname === dest.pathname && objectsAreEqual(toNumbers(queryString.parse()), toNumbers(dest.qObj));
};

module.exports = isDestinationMatchingCurrent;