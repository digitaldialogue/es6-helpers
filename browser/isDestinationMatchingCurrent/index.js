const queryString = require('../queryString');
const mapValues = require('../../object/mapValues');
const objectsAreEqual = require('../../object/areEqual');


const toNumbers = obj => mapValues(obj, val => Number.isNaN(Number(val))
  ? val
  : Number(val));

const isDestinationMatchingCurrent = (dest, curr = window.location) =>
  curr.origin === dest.origin
  && curr.pathname === dest.pathname
  && objectsAreEqual(toNumbers(queryString.parse()), toNumbers(dest.qObj));


module.exports = isDestinationMatchingCurrent;
