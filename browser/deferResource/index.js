module.exports = (type, obj) => document.head.appendChild(Object.assign(
  document.createElement(type),
  obj,
));
