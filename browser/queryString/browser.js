"use strict";

var qs = require('qs');

var stringify = function stringify(obj, options) {
  var string = qs.stringify(obj, options);
  return string ? "?".concat(string) : '';
};

var parse = function parse() {
  var string = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window.location.search;
  return qs.parse(string, {
    ignoreQueryPrefix: true
  });
};

module.exports = {
  stringify: stringify,
  parse: parse
};