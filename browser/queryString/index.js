const qs = require('qs');


const stringify = (obj, options) => {
  const string = qs.stringify(obj, options);
  return string
    ? `?${string}`
    : '';
};

const parse = (string = window.location.search) => qs.parse(string, { ignoreQueryPrefix: true });


module.exports = {
  stringify,
  parse,
};
