"use strict";

module.exports = function (err) {
  return setTimeout(function () {
    throw err;
  });
};