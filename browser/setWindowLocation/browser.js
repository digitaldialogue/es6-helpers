"use strict";

var queryString = require('../queryString');

var isDestinationMatchingCurrent = require('../isDestinationMatchingCurrent');

var setWindowLocation = function setWindowLocation(pathname) {
  var search = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : window.location.search;
  var origin = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : window.location.origin;
  if (isDestinationMatchingCurrent({
    origin: origin,
    pathname: pathname,
    qObj: queryString.parse(search)
  })) return false;
  window.location.href = "".concat(origin).concat(pathname).concat(search);
  return true;
};

module.exports = setWindowLocation;