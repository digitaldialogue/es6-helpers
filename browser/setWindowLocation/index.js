const queryString = require('../queryString');
const isDestinationMatchingCurrent = require('../isDestinationMatchingCurrent');


const setWindowLocation = (pathname, search = window.location.search, origin = window.location.origin) => {
  if (isDestinationMatchingCurrent({
    origin,
    pathname,
    qObj: queryString.parse(search),
  })) return false;
  
  window.location.href = `${origin}${pathname}${search}`;
  return true;
};


module.exports = setWindowLocation;
