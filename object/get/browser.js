"use strict";

var get = function get(key) {
  return function (obj) {
    return obj[key];
  };
};

module.exports = get;