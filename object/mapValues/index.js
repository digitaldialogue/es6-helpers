/**
 * Maps every value in an object without changing the keys.
 * Mapping function signature: (value, key, i, newObjectSoFar) => ...
 */
module.exports = (obj, fn) => Object.keys(obj).reduce((acc, key, i) =>
  ({ ...acc, [key]: fn(obj[key], key, i, acc) }), {});
