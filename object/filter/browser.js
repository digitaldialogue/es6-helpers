"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var filter = function filter(obj, filterFn) {
  return Object.keys(obj).reduce(function (newObj, key, i, keys) {
    return filterFn(key, i, keys) ? _extends(_defineProperty({}, key, obj[key]), newObj) : _extends({}, newObj);
  }, {});
};

module.exports = filter;