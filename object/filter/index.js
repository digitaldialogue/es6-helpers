const filter = (obj, filterFn) => Object.keys(obj).reduce((newObj, key, i, keys) => (filterFn(key, i, keys)
  ? Object.assign({ [key]: obj[key] }, newObj)
  : Object.assign({}, newObj)), {});


module.exports = filter;
