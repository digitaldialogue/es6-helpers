/**
 * Shallow checks that objects are equal (non-recursive).
 * @param {object} objA
 * @param {object} objB
 */
const areEqual = (objA, objB) => Object.keys(objB).every(keyB => Object.keys(objA).includes(keyB))
  && Object.entries(objA).reduce((result, [keyA, valA]) => result && valA === objB[keyA], true);


module.exports = areEqual;
