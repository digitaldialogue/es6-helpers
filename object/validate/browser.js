"use strict";

var isObject = function isObject(obj) {
  return Object.prototype.toString.call(obj).slice(8, -1) === 'Object';
};

var areObjects = function areObjects(objs) {
  var fnName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'every';
  return objs[fnName](isObject);
};

var allKeysMatch = function allKeysMatch(oOne, oTwo) {
  return Object.keys(oOne).every(function (key) {
    return Object.keys(oTwo).includes(key);
  });
};

var callbackIfObject = function callbackIfObject(oOne, oTwo, callback) {
  return areObjects([oOne, oTwo], 'some') ? callback(oOne, oTwo) : true;
};

var keyNotOpt = function keyNotOpt(key) {
  return !key.startsWith('_');
};

var hasOpt = function hasOpt(obj, opt) {
  return obj["_".concat(opt)];
};

var validateObjKeys = function validateObjKeys(obj, schema) {
  return hasOpt(schema, 'allowAdditionalKeys') || allKeysMatch(obj, schema);
};

var validateObjWithNesting = function validateObjWithNesting(obj, schema, callback) {
  return function (schemaKey) {
    return Object.keys(obj).includes(schemaKey) && callbackIfObject(obj[schemaKey], schema[schemaKey], callback);
  };
};

var validate = function validate(obj, schema) {
  return areObjects([obj, schema]) && (hasOpt(schema, 'skipKeys') || validateObjKeys(obj, schema) && Object.keys(schema).filter(keyNotOpt).every(validateObjWithNesting(obj, schema, validate)));
};
/**
 * Iterates over each key in the object to ensure the model object has the same keys.
 * Will iterate through any other nested objects accordingly.
 */


module.exports = validate;