module.exports = object => Object.entries(object).reduce(
  (obj, [key, val]) => ({ ...obj, [val]: key }),
  {},
);
